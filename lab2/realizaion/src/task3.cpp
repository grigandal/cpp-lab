#include <iostream>
#include <stdlib.h>
#include <ctime>
#include <stdexcept>
#include <string.h>
#include "task3.h"

int randomRange(int min, int max)
{
    return min + (std::rand() % (max - min + 1));
}

template <typename T>
void printCollection(Collection<T> *c)
{
    std::cout << c->className() << " [";
    for (int j = 0; j < c->getFill(); j++)
    {
        std::cout << c->get(j);
        if (j < c->getFill() - 1)
        {
            std::cout << " ";
        }
    }
    std::cout << "]" << std::endl;
}

void create(Collection<int> *cs[5])
{
    for (int i = 0; i < 5; i++)
    {
        if (i % 2)
        {
            cs[i] = new Stack<int>(randomRange(10, 20));
        }
        else
        {
            cs[i] = new Queue<int>(randomRange(10, 20));
        }
    }
}

void put(Collection<int> *cs[5])
{
    for (int i = 0; i < 5; i++)
    {
        std::cout << "---------writing---------" << std::endl;
        while (cs[i]->putable())
        {
            int e = randomRange(0, 50);
            std::cout << "putting " << e << " to " << i << "-th(st/nd/d) " << cs[i]->className() << " with length " << cs[i]->getLength() << " at index " << cs[i]->getFill() << std::endl;
            std::cout << "actual: ";
            cs[i]->put(e);
            printCollection<int>(cs[i]);
        }
        std::cout << std::endl;
    }
}

int main(int argc, char *argv[])
{
    std::srand(std::time(0));
    Collection<int> *cs[5];
    create(cs);
    put(cs);

    for (int i = 0; i < 5; i++)
    {
        printCollection<int>(cs[i]);
    }
    std::cout << std::endl;
    for (int i = 0; i < 5; i++)
    {
        std::cout << "---------reading---------" << std::endl;
        std::cout << "initial: ";
        printCollection(cs[i]);
        while (cs[i]->takeble())
        {
            std::cout << "taked " << cs[i]->take() << " from " << i << "-th(st/nd/d) " << cs[i]->className() << std::endl;
            std::cout << "remained: ";
            printCollection<int>(cs[i]);
        }
        std::cout << std::endl;
    }
    for (int i = 0; i < 5; i++)
    {
        delete cs[i];
    }
    return 0;
}