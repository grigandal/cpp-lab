#include <iostream>
#include "task2.h"

int main(int argc, char *argv[])
{
    Expr *myExpr = new Add(
        new Substract(
            new Number(1), new Number(2)),
        new Add(
            new Number(5),
            new Substract(
                new Number(8), new Number(3))));
    std::cout << "Получено выражение: " << myExpr->toString() << std::endl;
    std::cout << "Значение: " << myExpr->eval() << std::endl;

    delete myExpr;
    return 0;
}