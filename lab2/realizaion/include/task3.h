#include <stdexcept>
#include <string.h>
#include <iostream>

template <typename T>
class Collection
{
private:
protected:
    T *elements;
    int length;
    int fill;

public:
    Collection(int l);
    T get(int i);
    int getFill();
    int getLength();
    virtual std::string className()
    {
        return "collection";
    };
    bool putable();
    bool takeble();
    virtual void put(T e);
    virtual T take();
    ~Collection();
};

template <typename T>
class Stack : public Collection<T>
{
public:
    Stack(int l) : Collection<T>(l){};
    std::string className()
    {
        return "stack";
    };
    void put(T e);
    T take();
};

template <typename T>
class Queue : public Collection<T>
{
public:
    Queue(int l) : Collection<T>(l){};
    std::string className()
    {
        return "queue";
    };
    void put(T e);
    T take();
};

template <typename T>
Collection<T>::Collection(int l)
{
    this->elements = new T[0];
    this->length = l;
    this->fill = 0;
}

template <typename T>
T Collection<T>::get(int i)
{
    if (i < this->fill)
    {
        return this->elements[i];
    }
    else
    {
        throw new std::out_of_range(this->className() + " is not filled at index " + std::to_string(i));
    }
}

template <typename T>
int Collection<T>::getFill()
{
    return this->fill;
}

template <typename T>
int Collection<T>::getLength()
{
    return this->length;
}

template <typename T>
bool Collection<T>::putable()
{
    return this->fill < this->length;
}

template <typename T>
bool Collection<T>::takeble()
{
    return this->fill > 0;
}

template <typename T>
void Collection<T>::put(T e)
{
    if (!this->putable())
    {
        throw new std::out_of_range(this->className() + " is full to put");
    }
}

template <typename T>
T Collection<T>::take()
{
    if (!this->takeble())
    {
        throw new std::out_of_range(this->className() + " is empty");
    }
    else
    {
        this->fill--;
        T *newElements = new T[this->fill];
        for (int i = 0; i < this->fill; i++)
        {
            newElements[i] = this->elements[i];
        }
        delete[] this->elements;
        this->elements = newElements;
    }
}

template <typename T>
Collection<T>::~Collection()
{
    delete[] this->elements;
}

template <typename T>
void Stack<T>::put(T e)
{
    Collection<T>::put(e);
    this->fill++;
    T *newElements = new T[this->fill];
    for (int i = 0; i < this->fill - 1; i++)
    {
        newElements[i] = this->elements[i];
    }
    newElements[this->fill - 1] = e;
    delete[] this->elements;
    this->elements = newElements;
}

template <typename T>
T Stack<T>::take()
{
    T result = this->elements[this->fill - 1];
    Collection<T>::take();
    return result;
}

template <typename T>
void Queue<T>::put(T e)
{
    Collection<T>::put(e);
    this->fill++;
    T *newElements = new T[this->fill];
    for (int i = 1; i < this->fill; i++)
    {
        newElements[i] = this->elements[i - 1];
    }
    newElements[0] = e;
    delete[] this->elements;
    this->elements = newElements;
}

template <typename T>
T Queue<T>::take()
{
    T result = this->elements[this->fill - 1];
    Collection<T>::take();
    return result;
}