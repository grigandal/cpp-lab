#include <string>

class Expr
{
public:
    virtual int eval() = 0;
    virtual std::string toString() = 0;
};

class Number : public Expr
{
protected:
    int value;

public:
    Number(int value);
    int eval();
    std::string toString();
};

class BinaryOperation : public Expr
{
protected:
    Expr *left;
    Expr *right;

public:
    BinaryOperation(Expr *left, Expr *right);
    virtual int eval() = 0;
    virtual std::string toString() = 0;
    ~BinaryOperation();
};

class Add : public BinaryOperation
{

public:
    Add(Expr *left, Expr *right) : BinaryOperation(left, right){};
    int eval();
    std::string toString();
};

class Substract : public BinaryOperation
{

public:
    Substract(Expr *left, Expr *right) : BinaryOperation(left, right){};
    int eval();
    std::string toString();
};

Number::Number(int value) : Expr()
{
    this->value = value;
};

int Number::eval()
{
    return this->value;
};

std::string Number::toString()
{
    return std::to_string(this->value);
};

BinaryOperation::BinaryOperation(Expr *left, Expr *right) : Expr()
{
    this->left = left;
    this->right = right;
};

BinaryOperation::~BinaryOperation()
{
    delete this->left;
    delete this->right;
};

int Add::eval()
{
    return this->left->eval() + this->right->eval();
}

std::string Add::toString()
{
    return this->left->toString() + " + " + this->right->toString();
};

int Substract::eval()
{
    return this->left->eval() - this->right->eval();
}

std::string Substract::toString()
{
    return "(" + this->left->toString() + " - " + this->right->toString() + ")";
};