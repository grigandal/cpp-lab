class Parent
{
public:
    int param;
    Parent()
    {
        std::cout << "Конструктор родительского класса вызван" << std::endl;
        this->param = 0;
        std::cout << "Установлено поле 'param' = " << this->param << std::endl;
        std::cout << "Окончание конструктора родительского класса" << std::endl;
    };
    ~Parent(){
        std::cout << "Деструктор родительского класса вызван" << std::endl;
        std::cout << "Деструктор родительского класса окончен" << std::endl;
    };
};

class Child : public Parent
{
public:
    Child()
    {
        std::cout << "    Конструктор дочернего класса вызван" << std::endl;
        this->param = 5;
        std::cout << "    Установлено поле 'param' = " << this->param << std::endl;
        std::cout << "    Окончание конструктора дочернего класса" << std::endl;
    }
    ~Child(){
        std::cout << "    Деструктор дочернего класса вызван" << std::endl;
        std::cout << "    Деструктор дочернего класса окончен" << std::endl;
    };
};
