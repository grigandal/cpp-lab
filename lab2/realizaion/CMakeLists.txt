cmake_minimum_required(VERSION 3.0)
project(CPPLab2)

include_directories(include)

add_executable(task1 ./src/task1.cpp)
add_executable(task2 ./src/task2.cpp)
add_executable(task3 ./src/task3.cpp)
